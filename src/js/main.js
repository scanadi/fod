$(document).ready(function(){
    // Mobile Menu

    $('.menu-toggle').tap(function(event){
        event.preventDefault();
        $(this).toggleClass('bg-bviolet');
        $(this).find('i').toggleClass('fa-bars fa-close');
        $('.main-menu').toggleClass('lg-show');
    });

    $('.sub-tap').click(function( event ) {
        $(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
        var $this = $(this).parent().find('.sub-menu');
        $this.toggleClass('lg-show');
    });


    $('.home-nav a').click(function(event){
        event.preventDefault();
        $(this).parent().parent().find('li.active').removeClass('active');
        $(this).parent().addClass('active');
        var linkContent = $(this).text().toLowerCase();
        $('.tab-cont').addClass('hidden');
        $("#" + linkContent + "").removeClass('hidden')
    });


    // Ajax Search Json

    // Global search var
    var searchCont = $('.search');
    var searchVisible = !searchCont.hasClass("hide");

    // Show hide search
    function showHideSearch(){
        searchCont.toggleClass('hide').toggleClass('opacity');
        $('.bg-menu').toggleClass('fixed').toggleClass('col-12').toggleClass('top-0');
        $('.sub-menu').toggleClass('hide');
        $('body').toggleClass('fixed').toggleClass('col-12').toggleClass('top-0');

        // iF search is visible focus on input
        if(searchVisible) {
            $('.inputSearch').focus()
        } else {
            $('body').focus()
        }
    }

    // On x click reset the search input val
    function clearSearch(){
        $('.inputSearch').val('');
        doneTyping ();
    }

    // Toggle Search display
    $('.search-toggl').tap(function(event){
        event.preventDefault();

        if(searchVisible) {
            searchVisible = false
        } else {
            searchVisible = true
        }
        showHideSearch()
    });

    $('.clearSearch').tap(function(){
        if($input.val().length){
            clearSearch();
        }else {
            showHideSearch();
        }
    });

    // Search Container Height
    var menuHeight = $('.bg-menu').height();
    var searchHeaderHeight = $('.search-header').height();
    var searchContainer = $('.searchCont').height();
    var bodyHeight = $('body').height();
    var searchContInner =   $('.srchCont');
    var scrollHeight = bodyHeight - (searchHeaderHeight + menuHeight + searchContainer);

    function searchScrollBar(){
        searchContInner.height(scrollHeight);

    }

    searchScrollBar();

    $(window).resize(function(){
        searchScrollBar()
    });

    // Search App logic

    var typingTimer;                //timer identifier
    var doneTypingInterval = 500;  //time in ms, 5 second for example
    var $input = $('.inputSearch');
    var regex = new RegExp("[^a-zA-Z0-9]+", "gi");




    //on key up, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on key down, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something

    function doneTyping () {
        // Prepare input value for filtering search results
        var inputValue = $input.val().replace(regex, ' ').toLowerCase().trim().split(' ').filter(Boolean);
        var keyWords = [];
        var $searchHtml = "";

        $.each(inputValue, function(i){
            if(inputValue[i].length < 3) {
                return ;
            }
            keyWords.push(inputValue[i]);
        });

        if(inputValue.length){
            if($('.lang-sr').length){
                $searchHtml = "<div class='clearfix search-feed slatepink h2 py1 sm-py2 sm-px2'>... Učitava se </div>";
            } else {
                $searchHtml = "<div class='clearfix search-feed slatepink h2 py1 sm-py2 sm-px2'>... Loading </div>";
            }
        }

        $('.searchResults').html($searchHtml);

        function getSearch() {

            $searchHtml = "<div class='clearfix search-feed'>";
            var hasResults = 0;

            $.getJSON('system-pages/search.json', function(data) {

                $.each(data.pages, function(i, s) {
                    var itExists = false;
                    var allContent = s.title + " " + s.longtitle + " " + s.introtext + " " + s.content ;
                    allContent = allContent.toLowerCase();

                    $.each( keyWords, function(n){
                        if (allContent.indexOf(keyWords[n]) !== -1 ) {
                            itExists = true;
                            hasResults ++
                        }
                    });

                    var shortText = $.trim(s.introtext).substring(0, 320).split(" ").slice(0, -1).join(" ") + " ...";

                    if(itExists){
                        $searchHtml
                            += "<div class='sm-col sm-col-12 px0 sm-px2 mt0 sm-mt2 mb3 sm-mb2 search-item slatepink'>"
                              +  "<div class='h1'><a class='slatepink underline' href='" + s.rlink + "'>"+ s.title +"</a></div>"
                              +  "<div class='h3'>" + shortText + "</div>"
                            +  "</div>";
                    }

                });

                $searchHtml += '</div>';

                if(hasResults < 1 && keyWords.length) {
                    if($('.lang-sr').length){
                        $searchHtml = "<div class='clearfix search-feed slatepink h3 py1 sm-py2 sm-px2'>Nema rezultata za ovu pretragu </div>";
                    } else {
                        $searchHtml = "<div class='clearfix search-feed slatepink h3 py1 sm-py2 sm-px2'>No results for this search </div>";
                    }
                }

                $('.searchResults').html($searchHtml);

            });

        }
        getSearch();

        $('.searchResults').slimScroll({height: 'auto'});
    }

});
