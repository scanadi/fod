    'use strict';

    // Home Page slider

    //Global Variables
    var slideCont = $('.slider');
    var slideNumb = slideCont.children().length - 1;
    var elmSlides = slideCont.children();
    var currSlide = 0;
    var slideNext = $('.arrow-right');
    var slidePrev = $('.arrow-left');
    var slideDots = $('.slider-dots');
    var notLoaded = true;

    // Test to see if all the DOM is loaded if true set notLoaded to false
    function isLoaded() {
        if($(window).load() === true ){
            notLoaded = false;
        }
    }

    // Slider dots generation
    for( var i = 0 ; i < slideNumb + 1 ; i++ ){
        var n = i + 1;
        slideDots.append('<span class="bg-light dot pointer col mr1">'+ n +'</span>');
    }

    var allDots = slideDots.children();
    allDots.first().addClass('active');

    // Slider Controls
    allDots.click(function() {
        allDots.removeClass('active');
        $(this).addClass('active');
        currSlide = $(this).index();
        slider();
    });

    // Main Slider Function
    function slider(){

        // prevent the server request for loading.svg if all images are loaded
        isLoaded();

        // Slider Counter
        if(currSlide > slideNumb) {
            currSlide = 0;
        } else if (currSlide < 0) {
            currSlide = slideNumb;
        }

        //////// Slider URL manipulation and pre loader

        // Get current Slider background img url and remove the url()
        var currentImageUrl = elmSlides.eq(currSlide).data('img-url');
        // Remove class of the current slider for background size cover
        elmSlides.eq(currSlide).removeClass('bg-cover');

        // Replace the url with the loading SVG if image is not loaded
        if(notLoaded) {
            elmSlides.eq(currSlide).css('background-image', 'url("img/loading.svg")');
        }

        // Append an image to the body and cache it
        $('.images-preload').append('<img class="hide lazy-image" src="'+ currentImageUrl +'"/>');

        // Test to see if the image is loaded and if true replace the loading svg with the image
        $('.lazy-image').load( function(){
            // Get loaded img src
            var loadedImg =  $(this).attr('src');
            // Assign the loaded img src to the current slide
            elmSlides.eq(currSlide).css('background-image', 'url("'+ loadedImg +'")');
            // Add class of the current slider for background size cover
            elmSlides.eq(currSlide).addClass('bg-cover');
            $('.current').addClass('fadeBg');
            // removes the IMG tag that was appended
            $(this).remove();
        });

        // Slider Fade in and Fade out css animation
        elmSlides.removeClass('current').removeClass('fadein').addClass('fadeout');
        elmSlides.eq(currSlide).addClass('current').removeClass('fadeout').addClass('fadein');
        allDots[currSlide].click()
    }

    slideCont.on("swipeleft",function(){
        currSlide--;
        slider();
    });

    slideCont.on("swiperight",function(){
        currSlide++;
        slider();
    });

    slideNext.tap(function(){
        currSlide++;
        slider();
    });

    slidePrev.tap(function(){
        currSlide--;
        slider();
    });

    if(slideCont.length) {
        slider();
    }