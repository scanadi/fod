'use strict';

// Node Require

           var gulp = require('gulp'),
             concat = require('gulp-concat'),
             uglify = require('gulp-uglify'),
          minifyCss = require('gulp-minify-css'),
             rename = require('gulp-rename'),
         sourcemaps = require('gulp-sourcemaps'),
    convertEncoding = require('gulp-convert-encoding');

// Gulp Tasks definition

gulp.task('concatJS', function(){
    return gulp.src([
        'src/js/jquery-2.1.4.min.js',
        'src/js/jquery.mobile.min.js',
        'src/js/slider.js',
        'node_modules/photoswipe/dist/photoswipe.min.js',
        'node_modules/photoswipe/dist/photoswipe-ui-default.min.js',
        'node_modules/@staaky/strip/dist/js/strip.pkgd.min.js',
        'node_modules/jquery-slimscroll/jquery.slimscroll.js',
        'src/js/main.js',
        'src/js/google-analytics.js'
    ])
        .pipe(convertEncoding({to: 'utf8'}))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('copyHtml', function() {
    gulp.src('src/*.html')
    .pipe(gulp.dest('dist'));
});

gulp.task('minifyJS', ['concatJS'], function(){
    return gulp.src('dist/js/app.js')
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(convertEncoding({to: 'utf8'}))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('minifyCss', function() {
    return gulp.src('src/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('dist/css'));
});

// Watch, Build and Default

gulp.task('watchJs', function(){
    gulp.watch(['src/js/*.js'], ['minifyJS'])
});

gulp.task('watchCss', function(){
    gulp.watch(['src/css/*.css'], ['minifyCss'])
});

gulp.task('watchHtml', function(){
    gulp.watch(['src/*.html'], ['copyHtml'])
});

gulp.task('watch', ['watchJs', 'watchCss', 'watchHtml'], function(){
    console.log('Watching for changes');
});

gulp.task('build', ['minifyJS', 'minifyCss', 'copyHtml'], function(){
    console.log('Build Complete');
});

gulp.task('default', ['build'], function(){
    console.log('All Tasks Complete');
});